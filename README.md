# ebi-vue-components

Vue3 Components Library consisting of generic vue components used across EMBL-EBI

## Installation

```sh
npm install ebi-vue-components
```

Add following configuration to nuxt.config.ts

```javascript
app: {
        head: {
            script: [
                { src: 'https://ebi.emblstatic.net/web_guidelines/EBI-Framework/v1.4/js/script.js' }
            ],
            link: [
                { rel: 'stylesheet', href: 'https://assets.emblstatic.net/vf/v2.5.9/css/styles.css' },
                { rel: 'stylesheet', href: 'https://assets.emblstatic.net/vf/v2.4.12/assets/ebi-header-footer/ebi-header-footer.css' },
            ],
        }
    }
```

Create a nuxt plugin file `ebi-components.public.ts` in plugins directory, and add the following
```typescript
import index from "ebi-vue-components";

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use( index );
});
```

All set! Now directly use components created in this library in your code without adding any import.

## Contribution

In order to contribute to this package, create your component in components directory and explicitly export it `components/index.ts`

Add the component in `App.vue` to test it locally before publishing

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Publish to npm

```sh
npm publish
```