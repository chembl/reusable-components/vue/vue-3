// @ts-ignore
import EBIHeader from "./EBIHeader.vue";
import EBIFooter from "./EBIFooter.vue";
import BreadCrumbs from "./BreadCrumbs.vue";
import SearchResults from "./SearchResults.vue";
import FooterBanner from "./FooterBanner.vue";
import PrivacyBanner from "./PrivacyBanner.vue";
import VFHeader from "./VFHeader.vue";

export {
  EBIHeader,
  EBIFooter,
  BreadCrumbs,
  SearchResults,
  FooterBanner,
  PrivacyBanner,
  VFHeader,
};
